<?php

function get_connection(){
		$servername = "localhost";$username = "root";$password = "";$dbname = "adept";
		$conn = new mysqli($servername, $username, $password, $dbname);
		return $conn;
}

function select_items($selected_item){			     
			$conn = get_connection();
			$sql = "SELECT id, name FROM product";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {				 
				$select = '<select name="Product" class="form-control selectpicker">';
				$select.='<option value=" " >Please select product</option>';
				// output data of each row
				while($row = $result->fetch_assoc()) {
				  if ($selected_item == $row['id'])
					{
						$select.='<option selected value="'.$row['id'].'">'.$row['name'].'</option>';
				    }
				 else 
			     	{
						$select.='<option  value="'.$row['id'].'">'.$row['name'].'</option>';
         			}
				}
			} else {
				echo "0 results";
			}

			$select.='</select> ';
			$conn->close();
			echo $select;			 
  }  
  
  function select_suggestors($name){			     
			$conn = get_connection();
			$sql = "SELECT id, name FROM suggester";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
				$select= '<select name="suggester" class="form-control selectpicker">';
				$select.='<option value=" " >Module Suggested By</option>';
				// output data of each row
				while($row = $result->fetch_assoc()) {
					if( $name == $row['name'] )
					{
						$select.='<option selected value="'.$row['id'].'">'.$row['name'].'</option>';
					}
					else 
					{
						$select.='<option value="'.$row['id'].'">'.$row['name'].'</option>';
					}
				}
			} else {
				echo "0 results";
			}

			$select.='</select>';
			$conn->close();
			echo $select;			 
  } 
  
  
 function get_customers_dropdown(){
	 $conn = get_connection();
	 $select = "";
			$sql = "SELECT id,name , phone , email  FROM customer";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
				$select= '<input oninput="onInput()" list="persons" id="person" >';
				$select.='<datalist id="persons">';
				// output data of each row
				while($row = $result->fetch_assoc()) {
					//echo "id: " . $row["id"]. " - Name: " . $row["name"]. " " . $row["name"]. "<br>";
					$select.='<option data-id="'.$row['id'].'" data-email="'.$row['email'].'" data-name="'.$row['name'].'" data-phone="'.$row['phone'].'"  value="'.$row['name'].'"> ';
				}
			} else {
				//echo "0 results";
			}

			$select.='</select>';
			$conn->close();
			echo $select;	
 }  

function priority_count(){
			$conn = get_connection();
			$sql = "SELECT count(id) as 'count'  FROM temporary_customers";
			$result = $conn->query($sql);
			$row = $result->fetch_row();
			 
			$conn->close();
			echo json_encode($row);
} 
 
 function fill_form($module_id){	
	$conn = get_connection();
	$sql = "SELECT m.id, title , m.product_id , description , s.Name as suggested_by , p.name as product , module_type FROM module as  m JOIN product as p ON m.product_id = p.id JOIN suggester as s ON m.suggested_by = s.id where m.id = ".$module_id." ";
	$result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
	
	 // code for filling temporary_customers start 
	   // first truncate the table 
		  $sql = "TRUNCATE TABLE  temporary_customers ";
		  $conn->query($sql);
	    
	   // fill temporary table 
		  $sql = "insert into temporary_customers (name , phone, email ,customer_id) SELECT c.name , c.phone , c.email , c.id as customer_id FROM customer as c INNER JOIN module_customers as mc on c.id = mc.customer_id where mc.module_id = ".$module_id." ";
		  $conn->query($sql);
	 // code for filling temporary_customers end
	 
	return $row; //return first data
} ?> 


<?php  function get_module_id($module_latest_id){
	if($module_latest_id == "")    // new
	{
		$conn = get_connection();
		
		$sql = "TRUNCATE TABLE  temporary_customers ";  
		$conn->query($sql); //execute the query
				
		$sql = "SELECT  count(id)+1 FROM module";
		$result = $conn->query($sql);
		$row = $result->fetch_row();	 
		$conn->close();	 
		echo implode(" ",$row);
	}
	else                         // edit
	{
		echo $module_latest_id;
	}

}?>
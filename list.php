<!DOCTYPE html>
<html lang="en">
<head>     
	<?php include("headers/header.php") ?> 
	<style>
			table {
				font-family: arial, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}

			td, th {
				border: 1px solid #dddddd;
				text-align: left;
				padding: 8px;
			}

			tr:nth-child(even) {
				background-color: #dddddd;
			}
	</style>
			
</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Development Priority Module</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/adept">Home</a></li>       
      <li class="active"><a href="list.php">Manage Modules </a></li>
      <li><a href="#">Admin Section</a></li>
    </ul>
	<ul class="nav navbar-nav navbar-right">
		<li>
			 
		</li>
	</ul>
  </div>
</nav>
   
<div class="container">
  <br/><br/><br/>   
 <?php include("db.php") ?>    
 <?php echo ' <script type="text/javascript"> setTimeout(function(){ onload_list(""); }, 800); </script>'; ?> 
 
 
  <div class="well well-sm"   style="text-align:center;"> <h3><b> Modules </b></h3></div>
  <input type="text"   id="searchBox" name="search" placeholder="Search.."> <br/> <br/> 
  
  <div class="div_modules"> </div>
   
  
</div><!-- /.container -->

</body>

</html>
<?php include("db.php"); 
$conn = get_connection();

if(isset($_GET["action"]))
{	
	$function2call =  htmlspecialchars($_GET["action"]); 
}
else if(isset($_POST["action"]))
{	
	$function2call =  htmlspecialchars($_POST["action"]);    
}
 
 switch($function2call) {
        case 'get_customers' : getcustomerList($conn);break;	 
        case 'get_priority' : get_priority($conn) ; break;
		case 'add_customers' : add_customers($conn) ; break;
		case 'remove_customer' : remove_customer($conn , $_POST["id"]); break;
		case 'remove_module' : remove_module($conn , $_POST["id"]); break;
		case 'insert_module' :  insert_module($conn , $_POST["form"] ); break;
		case 'get_module_list' :  getmodulelist($conn , $_POST["title"] ); break;
        // other cases
    }

// getcustomerList start
function getcustomerList($conn) {
	$sql = "SELECT id,name , phone , email  FROM temporary_customers";
	$result = $conn->query($sql);			 
if ($result->num_rows > 0) { 
 ?>
		<!DOCTYPE html>
		<html>
		<head>
		<style>
			table {
				font-family: arial, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}

			td, th {
				border: 1px solid #dddddd;
				text-align: left;
				padding: 8px;
			}

			tr:nth-child(even) {
				background-color: #dddddd;
			}
		</style>
		</head>
		<body>
		<div >
			<div class="well well-sm"   style="text-align:center;"> Customers</div>
			<table class="table table-hover">
			  <tr>
				<th>Name</th>			 
				<th>Email</th>
				<th>Action</th>
			  </tr>
			  
			  <?php while($row = $result->fetch_assoc()) { ?>
				  <tr>
					<td><?php echo$row['name']?></td>				
					<td><?php echo$row['email']?></td>
					<td><?php echo "<button type='button' class='btn btn-danger'  onclick="."removeCustomer('".$row['id']."')"."  > Remove </button >" ?></td>					 
				  </tr>
			  <?php } ?>
			</table>
        </div>
		</body>
		</html>
       
<?php  $conn->close();
    }

} 
// getcustomerList end  

// get_priority start

function get_priority($conn){
	$sql = "SELECT count(id) as 'count'  FROM temporary_customers";
	$result = $conn->query($sql);			 
	$row = $result->fetch_row();			 
	$conn->close();	 
	echo implode(" ",$row);
}	
// get_priority end
?>

<?php // add_customers start
  function add_customers($conn){
	  
	$cust_phone="";
	$cust_name="";
	$cust_email="";
    $deserial = "";
	if(isset($_POST['form']))
	{		 
		parse_str($_POST['form'], $deserial);	 // setting array	     
		$cust_name = $_POST['newC_name'];
		$cust_email = $_POST['newC_email'];
		$cust_phone = $_POST['newC_phone'];
	} 	 
	$conn = get_connection();

    if (trim($deserial['cust_id']) != '')     // check if name is not blank
	{ 	
	    $counted =find_customer_by_id( $conn ,$deserial['cust_id']);		 
		if($counted == '0'){
				$sql = "INSERT INTO temporary_customers (customer_id,name,email,phone) VALUES ('".$deserial['cust_id']."', '".$deserial['cust_name']."', '".$deserial['cust_email']."', '".$deserial['cust_phone']."')";      	  
				if ($conn->query($sql) === TRUE) {	 
				} else {
					echo "Error: " . $sql . "<br>" . $conn->error;
				}
				$conn->close();
		} else {
			echo  json_encode("_repeated*");
		}
	}
	else if  ((trim($deserial['cust_id']) == '') && (trim($cust_name != "" )))
	{
		$sql = "INSERT INTO customer (name,email,phone) VALUES ('".$cust_name."', '".$cust_email."', '".$cust_phone."')";      	  
		if ($conn->query($sql) === TRUE) {
			 $cust_id = $conn->insert_id ;   //last inserted id      
			 $sql = "INSERT INTO temporary_customers (customer_id,name,email,phone) VALUES ('".$cust_id."', '".$cust_name."', '".$cust_email."', '".$cust_phone."')";      	  
			 $conn->query($sql);	 	 
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
		$conn->close();
	}
  }
   
// add_customers end

   function find_customer_by_id( $conn , $id)
   {	   
		$sql = "Select count('id') from temporary_customers where customer_id = '".$id."' "; 
	    $result = $conn->query($sql);
	    $row = $result->fetch_row();	 		  
	    $counted = implode(" ",$row);
		return $counted;
   }
?>



<?php // remove customer
	function remove_customer($conn, $id)
	{
		$sql = "Delete from temporary_customers where id= ".$id."";
		if ($conn->query($sql) === TRUE) {
				echo "_Deleted*";
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
		$conn->close();		 		 
	}
?>

<?php // remove module
    function remove_module($conn , $id)
	{
		$sql = "Delete from module_customers where module_id= ".$id."";  //child table delete
		$conn->query($sql);
		$sql = "Delete from module where id= ".$id."";
		if ($conn->query($sql) === TRUE) {			  
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
		$conn->close();	
	}
?>

<?php   // insert module into db
	function insert_module($conn , $form)
	{ print_r($form);
		$deserial = "";		 
		parse_str($form , $deserial);		 
		  $module_id  = $deserial['module_id'];
	      $sql = "Select count('id') from module where id = '".$module_id."' "; 
		  $result = $conn->query($sql);
		  $row = $result->fetch_row();	 		  
		  $counted = implode(" ",$row);		  
		if($counted == '0')  // then insert
		{
		//	insert module
			$sql = "INSERT INTO module (title,	description , suggested_by , product_id , module_type ) VALUES ('".$deserial['title']."', '".$deserial['comment']."', '".$deserial['suggester']."'  ,  '".$deserial['Product']."'  ,  '".$deserial['newold']."'  )";      	  		 
			if ($conn->query($sql) === TRUE) {
				$module_id = $conn->insert_id ;   //last inserted id            			 
				$sql = " INSERT INTO module_customers (customer_id , module_id ) SELECT customer_id , (SELECT '".$module_id."' as module_id) FROM temporary_customers ";			 
				$conn->query($sql); //execute the query
				
					// delete temporary_customers
				$sql = "TRUNCATE TABLE  temporary_customers ";
				$conn->query($sql); //execute the query
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		else 
		{
			$sql = "update module set title = '".$deserial['title']."' ,	description  = '".$deserial['comment']."' , suggested_by = '".$deserial['suggester']."' , product_id = '".$deserial['Product']."' , module_type = '".$deserial['newold']."'   where id = '".$module_id."' ";      	  		 			 
			if ($conn->query($sql) === TRUE) {
				$sql = " Delete from module_customers where module_id   = '".$module_id."' ";				  				
				$conn->query($sql); //execute the query
				
				$sql = " INSERT INTO module_customers (customer_id , module_id ) SELECT customer_id , (SELECT '".$module_id."' as module_id) FROM temporary_customers ";			 
				$conn->query($sql); //execute the query
				
					// delete temporary_customers
				$sql = "TRUNCATE TABLE  temporary_customers ";
				$conn->query($sql); //execute the query
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		$conn->close();	 	 					 		    
	}
?>

<?php 
  function getmodulelist($conn , $title){ 
        if(trim($title) == "")		
		{
			$sql = "SELECT (select count(*) from module_customers where module_id=m.id) AS 'Priority',m.id, title , description , s.Name as suggested_by , p.name as product FROM module as m JOIN product as p ON m.product_id = p.id JOIN suggester as s ON m.suggested_by = s.id order by 1 desc ";
		}
		else 
		{
			$sql = "SELECT (select count(*) from module_customers where module_id=m.id) AS 'Priority',m.id, title , description , s.Name as suggested_by , p.name as product FROM module as m JOIN product as p ON m.product_id = p.id JOIN suggester as s ON m.suggested_by = s.id where m.title LIKE  '%".$title."%' order by 1 desc ";	
		}
		
		$result = $conn->query($sql);	     
		if ($result->num_rows > 0)
		{ ?>		 
                   <table class="table table-hover">
							  <tr>
								<th>Title </th>			 
								<th>Priority </th>	
								<th>Description</th>
								<th>Suggested By</th>
								<th>Product </th>
								<th>Action</th>
							  </tr>
							  
									  <?php while($row = $result->fetch_assoc()) { ?>
										  <tr>
											<td><?php echo$row['title']?></td>														 		
											<td><?php echo$row['Priority']?></td>				
											<td><?php echo$row['description']?></td>
											<td><?php echo$row['suggested_by']?></td>
											<td><?php echo$row['product']?></td>
											<td> &nbsp;			     &nbsp;			     
												<a   class='btn btn-info'  href="index.php?module_id=<?php echo $row['id']; ?>" > Edit  <span class="glyphicon glyphicon-edit"></span> </a > &nbsp; 
												<a  class='btn btn-danger'  onclick="removeModule('<?php echo $row['id']; ?>')" > Delete <span class="glyphicon glyphicon-trash"></span>   </a >
											</td>					 
										  </tr>
									  <?php } ?>
					</table>						 
			 <?php
				$conn->close();		 
		}
		else 
		{ ?>			  
					<table class="table table-hover">
						  <tr>
							<th>Title </th>	
							<th>Priority </th>							
							<th>Description</th>
							<th>Suggested By</th>
							<th>Product </th>
							<th>Action</th>
						  </tr>
					</table>							  
	<?php }   
  }
?>







 <title>Development Priority Module</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js"></script>
  <style> 
		#success_message{ display: none;}
  </style>
  <link rel="stylesheet" type="text/css" href="css/allstyles.css">
  <script src="js/index.js"> </script>     
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.0/pnotify.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.0/pnotify.css" media="all" rel="stylesheet" type="text/css" />
  
  <script>
		function myAjax() {
		$.ajax({
			   type: "GET",
			   url: 'customers.php',
			   async : true ,
			   data:{action:'get_customers'},
			   success:function(html) {
				   $("body").animate({ scrollTop: window.pageYOffset + $(window).height()}, 300);
				   $(".customers" ).html(html);
				   get_priority_count();				    
			   }
				
			});			 
		};		
		
		function get_priority_count(){
			$.ajax({
			   type: "GET",
			   url: 'customers.php',	
			   data:{action:'get_priority'},			   
			   success:function(data) { 
				   $("#priority_count" ).html(data);
			   }
				
			});	
		};
		
		function add_customer(){
			var formdata = $('form').serialize();
		    var newC_name = document.getElementById("person").value;	 			
			var newC_phone= document.getElementById("cust_phone").value;	 			
			var newC_email = document.getElementById("cust_email").value;	 						 
			$.ajax({
			   type: "POST",
			   url: 'customers.php',
			   data: {action : 'add_customers', form :formdata , newC_name : newC_name , newC_phone : newC_phone , newC_email : newC_email  },
			   success:function(data) { console.log(data);
				  var result = data.split('_').pop().split('*').shift(); // returns 'repeated'			        
				  call_pnotify(result);				
				  myAjax();				  
			   }

			});			 
		};			 
		 
		 
		 function call_pnotify(result){
			 PNotify.prototype.options.styling = "bootstrap3";
				PNotify.prototype.options.delay = 500;
				
				if(result == "repeated"){
					$(document).ready(function(){
						new PNotify({
							title: 'Warning',
							text: 'This customer has already been added.',
							type: 'error' 
						});
					});
				};
				
				if(result == "Deleted"){
					$(document).ready(function(){
						new PNotify({
							title: 'Removed',
							text: 'This customer has been removed.',
							type: 'info' 
						});
					});
				}
		 };
  </script>
  <script> 
	function onInput() {
		var val = document.getElementById("person").value;	        
        var set = document.querySelector("#persons option[value='"+val+"']"); 		 
		if(set != null)
		{  
		    var dataset = set.dataset;  // init dataset
			var cust_phone=dataset.phone;		  
			var cust_name=dataset.name;		   
			var cust_email=dataset.email;
            var cust_id=dataset.id;			
			$("input[name=cust_phone]").val(cust_phone);
			$("input[name=cust_name]").val(cust_name);
			$("input[name=cust_email]").val(cust_email);			 
			$("input[name=cust_id]").val(cust_id);
		}
		else
		{
			$("input[name=cust_phone]").val('');			 
			$("input[name=cust_email]").val('');
			$("input[name=cust_id]").val('');
		   // console.log(document.getElementById("cust_name").value);		
		}
  } ;
 
  </script>
   
  <!-- remove  Function-->
  <script>
	function  removeCustomer(id){
		 
		 $.ajax({
			   type: "POST",
			   url: 'customers.php',
			   data:{action:'remove_customer' , id:id},
			   success:function(data) { console.log(data);	
			      var result = data.split('_').pop().split('*').shift(); // returns 'repeated'			        
				  call_pnotify(result);	
				  
				   myAjax();
				   get_priority_count();
			   }
				
			});			 		 
		return false;
	};
	
	function removeModule(id){
		 $.ajax({
			   type: "POST",
			   url: 'customers.php',
			   data:{action:'remove_module' , id:id},
			   success:function(data) { console.log(data);	
				   myAjax();
				   get_priority_count();	
				   onload_list(''); 				   
			   }
				
			});			 		 
		return false;
	};
  </script>
  
  <script> // for list.php
		$(document).ready(function(){
			
			 // on change event
			 $('#searchBox').keyup(function() { 
				var val = $(this).val(); // get the current value of the input field.
				onload_list(val);  
			});
			// on change event
			
		});
		
		 
		function onload_list(title){
			$.ajax({
			   type: "POST",
			   url: 'customers.php',
			   data:{action:'get_module_list' , title:title},
			   success:function(data) {  	
				 	$(".div_modules" ).html(data);			   
			   }
				
			});			 		 
			return false;
		};		 
  </script>
<!DOCTYPE html>
<html lang="en">
<head>     
	<?php include("headers/header.php") ?>  	
</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Development Priority Module</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/adept">Home</a></li>       
      <li><a href="list.php">Manage Modules </a></li>
      <li><a href="#">Admin Section</a></li>
    </ul>
	<ul class="nav navbar-nav navbar-right">
		<li>
				<h2 class="badge" style="background-color:green;" >Priority 				 
					<span id="priority_count" class="badge" style="background-color:red;">
						 0
					</span>				 
				</h2> 
		</li>
		<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
    </ul>
  </div>
</nav>

<div class="container"> 
  <?php include("db.php") ?>     
  <?php 
		$title = "";
		$description = "";
        $selected_item = "";	
	    $selected_suggestor = "";
        $module_latest_id = "";
		$newold = "New";
		if (strpos($_SERVER['REQUEST_URI'], "module_id") !== false){			 
			$module_id = htmlspecialchars($_GET["module_id"]);  			 
			$result = fill_form($module_id);	 // filling sticky form ********
			$title = $result['title'];	
            $selected_item = $result['product_id'];			
			$selected_suggestor = $result['suggested_by'];
			$description = $result['description'].trim();
		    $newold = $result['module_type'];
			$module_latest_id = $result['id'] ;				 			 			 
			echo ' <script type="text/javascript"> setTimeout(function(){ myAjax(); }, 1000); </script>'; 
		}
  ?>  
  
    <form class="well form-horizontal" action="customers.php" - method="post"  id="contact_form">
<fieldset>

<!-- Form Name -->
 
<!-- Text input-->
 

<!-- Text input-->
 <br/><br/><br/>
<div class="form-group">
  <label class="col-md-4 control-label">Requirement Number</label>  
  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
		<input name="module_id" readonly value="<?php get_module_id($module_latest_id) ;?>" class="form-control"  type="text">
    </div>
  </div>
  <div class="col-md-4  pull-right"> 
		<div class="pull-right">	
								 
		</div>
  </div>
</div>

<!-- title -->
  
<div class="form-group">
  <label class="col-md-4 control-label"> Title</label>
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
        	<input class="form-control" name="title" value="<?php echo $title; ?>" type="text" placeholder=" Title"> 
  </div>
  </div>
</div>

<br/>
<!-- Select Basic -->
   
<div class="form-group"> 
  <label class="col-md-4 control-label">Product</label>
    <div class="col-md-4 selectContainer">
		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>     
				<?php select_items($selected_item);?>
		</div>
    </div>
</div>

<!-- Text input--> 
<br/>
 
<!-- radio checks -->
 <div class="form-group">
                        <label class="col-md-4 control-label">Module</label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="newold"  checked="checked"  value="New" 
										 <?php if ( $newold == "New"){ echo 'checked';} ?>
									/> New
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="newold" value="Old" 
										<?php if ( $newold == "Old"){ echo 'checked';} ?>
									/> Old
                                </label>
                            </div>
                        </div>
                    </div>

<br/>
 
<!-- Text area -->
  
<div class="form-group">
  <label class="col-md-4 control-label"> Description</label>
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
        	<textarea class="form-control" name="comment" placeholder=" Description"><?php echo $description; ?></textarea>
  </div>
  </div>
</div>  

<br/>
 
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label">Suggested BY</label>
	<div class="col-md-4 selectContainer">
		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>     
				<?php select_suggestors($selected_suggestor); ?>
		</div>
    </div>
</div> 

<br/>
 

<!-- Button -->
<div class="well well-sm" style="text-align:center;">
   Add Customers
</div> 

<br/>
<!-- customer add -->
<div class="form-group">   
  <div class="col-md-2 col-md-offset-1">
	 <p> Customer Name</p>
  </div>
  <div class="col-md-2 col-md-offset-1">
     <p> Customer Phone</p>
  </div>  
  <div class="col-md-2 col-md-offset-1">
	 <p> Customer Email</p>
  </div>   
</div> 


<div class="form-group">   
  <div class="col-md-1 col-md-offset-1">
	 <?php get_customers_dropdown(); ?>	 
	 <input name="cust_name" id="cust_name" value="" type="text">
  </div>
  <div class="col-md-1 col-md-offset-2">
     <input name="cust_phone" id="cust_phone"  type="text">
  </div>
  <div class="col-md-1 col-md-offset-2">
     <input name="cust_email" id="cust_email"  type="text">
	 <input name="cust_id"    type="hidden"> <!--Hidden Type ID-->
  </div>
  <div class="col-md-2 col-md-offset-2">
      <button type="submit" onclick="add_customer(); return false;" class="btn btn-danger">Add Customer  <span class="glyphicon glyphicon-user"></span></button>
  </div>
</div> 
<!-- customer add -->
 
 <hr/><br/><br/>
 <!--Add Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4">
    <button type="submit"    class="btn btn-warning" >Submit Module <span class="glyphicon glyphicon-send"></span></button>
  </div>
</div>

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Module Has been Added</div>

</fieldset>
</form>

<div class="customers"> </div>
</div>
   
</div><!-- /.container -->

</body>

</html>
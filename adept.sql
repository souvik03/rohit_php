-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2017 at 09:22 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adept`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `name` varchar(200) NOT NULL,
  `id` int(11) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`name`, `id`, `phone`, `email`, `type`) VALUES
('rohit', 38, '1sdfdf', 'r@example.com', ''),
('anushua', 39, '2', 'jdfhn@example.comd', ''),
('adept', 40, '33', 'pohjhj@example.com', ''),
('ram', 41, '13434', '34gghh@example.com', ''),
('shera', 71, '24', 'sh@gmail.cm', ''),
('remiya', 72, '5456', 'rem@gmail.com', ''),
('hukum singh', 73, '454545', 'hs@mail.com', ''),
('souvijk', 76, '434', 'rdf', ''),
('souvik', 77, '454554', 'sdsdgfg@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `suggested_by` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `module_type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `title`, `description`, `suggested_by`, `product_id`, `module_type`) VALUES
(51, 'Testing 2', 'This is just for testing purpose', 1, 3, 'Old'),
(52, 'Testing 3', 'asdfasdfasdf', 2, 1, 'Old'),
(54, 'new title', 'total desc', 1, 2, 'Old');

-- --------------------------------------------------------

--
-- Table structure for table `module_customers`
--

CREATE TABLE `module_customers` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_customers`
--

INSERT INTO `module_customers` (`id`, `module_id`, `customer_id`) VALUES
(298, 51, 39),
(299, 51, 40),
(300, 51, 41),
(301, 51, 38),
(302, 51, 71),
(303, 51, 72),
(305, 54, 38),
(306, 54, 71),
(315, 52, 72),
(316, 52, 38),
(317, 52, 41),
(318, 52, 73),
(319, 52, 71);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`) VALUES
(1, 'XBRL'),
(2, 'IPRO'),
(3, 'TDS'),
(4, 'INVEST PRO');

-- --------------------------------------------------------

--
-- Table structure for table `suggester`
--

CREATE TABLE `suggester` (
  `id` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggester`
--

INSERT INTO `suggester` (`id`, `Name`) VALUES
(1, 'Pranoy'),
(2, 'Anushua');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_customers`
--

CREATE TABLE `temporary_customers` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `customer_id` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `suggested_by` (`suggested_by`);

--
-- Indexes for table `module_customers`
--
ALTER TABLE `module_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggester`
--
ALTER TABLE `suggester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_customers`
--
ALTER TABLE `temporary_customers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `module_customers`
--
ALTER TABLE `module_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=322;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suggester`
--
ALTER TABLE `suggester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `temporary_customers`
--
ALTER TABLE `temporary_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `module_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `module_ibfk_2` FOREIGN KEY (`suggested_by`) REFERENCES `suggester` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `module_customers`
--
ALTER TABLE `module_customers`
  ADD CONSTRAINT `module_customers_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `module_customers_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
